using AutomationFramework.CoreCommon;
using NUnit.Framework;

namespace AutomationFramework.Test
{
    [TestFixture("chrome", "WINDOWS")]
    [TestFixture("edge", "WINDOWS")]
    [Parallelizable(ParallelScope.Fixtures)]
    public class SimpleTest : BaseTest
    {
        public SimpleTest(string browser, string osPlatform) : base(browser, osPlatform)
        {
        }

        [Test]
        [Parallelizable]
        [Category("Study")]
        public void SimpleTest_01()
        {
            webDriver.Url = "http://google.com";
        }

        [Test]
        [Parallelizable]
        [Category("Study")]
        public void SimpleTest_02()
        {
            webDriver.Url = "http://w3school.com";
        }

        [Test]
        [Parallelizable]
        [Category("Article")]
        public void SimpleTest_03()
        {
            webDriver.Url = "http://tuoitre.vn";
        }

        [TearDown]
        public void TearDown()
        {
            webDriver.Quit();
        }
    }
}