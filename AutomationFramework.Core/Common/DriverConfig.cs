﻿using AutomationFramework.CoreEnum;

namespace AutomationFramework.CoreCommon
{
    public class DriverConfig
    {
        public string browserName;
        public string osPlatform;
        public DriverType driverType;
    }
}
