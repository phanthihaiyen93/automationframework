﻿using AutomationFramework.CoreDriver;
using AutomationFramework.CoreEnum;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AutomationFramework.CoreCommon
{
    public class BaseTest
    {
        private readonly string _browser;
        private readonly string _osPlatform;

        public IWebDriver webDriver { get; set; }

        public BaseTest(string browser, string osPlatform)
        {
            _browser = browser;
            _osPlatform = osPlatform;
        }

        [SetUp]
        public void Setup()
        {
            var driverConfig = new DriverConfig
            {
                browserName = _browser,
                osPlatform = _osPlatform,
                driverType = DriverType.GridDriver
            };

            var driver = DriverFactory.GetWebDriver(driverConfig);
            webDriver = driver.CreateDriver();
        }
    }
}
