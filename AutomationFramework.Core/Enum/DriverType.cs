﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationFramework.CoreEnum
{
    public enum  DriverType
    {
        LocalDriver,
        GridDriver,
        SauceLabDriver,
        BrowserStackDriver
    }
}
