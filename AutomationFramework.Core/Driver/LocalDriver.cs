﻿using AutomationFramework.CoreCommon;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using System;
using System.Linq;
using WebDriverManager.DriverConfigs.Impl;

namespace AutomationFramework.CoreDriver
{
    public class LocalDriver : IDriver
    {
        DriverConfig _driverConfig;

        public LocalDriver(DriverConfig driverConfig) 
        {
            _driverConfig = driverConfig;
        }

        public IWebDriver CreateDriver()
        {
            IWebDriver driver = null;

            var browserName = _driverConfig.browserName;

            if (browserName.SequenceEqual("firefox"))
            {
                new WebDriverManager.DriverManager().SetUpDriver(new FirefoxConfig());
                driver = new FirefoxDriver();
            }
            else if (browserName.SequenceEqual("chrome"))
            {
                new WebDriverManager.DriverManager().SetUpDriver(new ChromeConfig());
                driver = new ChromeDriver();
            }
            else if (browserName.SequenceEqual("edge"))
            {
                new WebDriverManager.DriverManager().SetUpDriver(new EdgeConfig());
                driver = new EdgeDriver();
            }

            driver.Manage().Window.Maximize();
            return driver;
        }

    }
}
