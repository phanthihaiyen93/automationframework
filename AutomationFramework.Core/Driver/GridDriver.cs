﻿using AutomationFramework.CoreCommon;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using System;
using System.Linq;

namespace AutomationFramework.CoreDriver
{
    public class GridDriver : IDriver
    {
        DriverConfig _driverConfig;

        public GridDriver(DriverConfig driverConfig)
        {
            _driverConfig = driverConfig;
        }

        public IWebDriver CreateDriver()
        {
            IWebDriver driver = null;
            var browserName = _driverConfig.browserName;
            if (browserName.SequenceEqual("chrome"))
            {
                ChromeOptions Options = new ChromeOptions();
                Options.PlatformName = "WINDOWS";
                driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), Options.ToCapabilities());
            }
            else if (browserName.SequenceEqual("firefox"))
            {
                FirefoxOptions Options = new FirefoxOptions();
                Options.PlatformName = "WINDOWS";
                driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), Options.ToCapabilities());
            }

            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            return driver;
        }
    }
}
