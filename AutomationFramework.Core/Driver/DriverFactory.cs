﻿using AutomationFramework.CoreCommon;
using AutomationFramework.CoreEnum;

namespace AutomationFramework.CoreDriver
{
    public class DriverFactory
    {
        public static IDriver GetWebDriver(DriverConfig driverConfig)
        {
            switch(driverConfig.driverType)
            {
                case DriverType.LocalDriver:
                    {
                        return new LocalDriver(driverConfig);
                    }
                case DriverType.GridDriver:
                    {
                        return new GridDriver(driverConfig);
                    }

                default:
                    return new LocalDriver(driverConfig);
            }
        }
    }
}
