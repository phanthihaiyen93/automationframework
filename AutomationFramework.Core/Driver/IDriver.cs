﻿using AutomationFramework.CoreCommon;
using OpenQA.Selenium;

namespace AutomationFramework.CoreDriver
{
    public interface IDriver
    {
        IWebDriver CreateDriver();
    }
}
